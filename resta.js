const express = require('express')
const bodyParser = require("body-parser");
const app = express()
const port = 3000

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/resta', (request, response) => {
    var n1 = parseInt(request.body.numero1)
    var n2 = parseInt(request.body.numero2)
    var res = n1-n2
    response.send(res+"")
})

app.listen(port, () => {
  console.log(`Servidor corriendo en el puerto ${port}`)
})