const express = require('express')
const app = express()
const port = 4000

app.get('/', (request, response) => {
    response.send('Saul Absalon Barillas Argueta - 201807160')
})

app.listen(port, () => {
  console.log(`Servidor corriendo en el puerto ${port}`)
})